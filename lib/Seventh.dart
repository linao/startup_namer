import 'package:flutter/material.dart';

class Seventh extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        body: new Container(
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: _buildLeftLayout(),
        ),
      ),
    );
  }

  //构建左边
  Widget _buildLeftLayout() {
    return new Container(
      child: new Column(
        children: <Widget>[
          new Container(
            padding: EdgeInsets.all(40),
            child: new Text(
              'Strawberry Pavlova',
              style: new TextStyle(
                color: Colors.black,
                fontSize: 18,
              ),
            ),
          ),
          new Container(
            child: new Container(
              padding: EdgeInsets.all(20),
              child: new Text(
                '也许你想要一个widget占据其兄弟widget两倍的空间。'
                    '您可以将行或列的子项放置在Expandedwidget中，'
                    ' 以控制沿着主轴方向的widget大小。'
                    'Expanded widget具有一个flex属性，'
                    '它是一个整数，用于确定widget的弹性系数'
                    ',默认弹性系数是1。',
                style: new TextStyle(
                  color: Colors.black45,
                  fontSize: 15,
                ),
                softWrap: true,
                textAlign: TextAlign.center,
              ),
            ),
          ),
          new Container(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Container(
                  child: new Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Icon(
                        Icons.star,
                        color: Colors.red,
                      ),
                      new Icon(
                        Icons.star,
                        color: Colors.red,
                      ),
                      new Icon(
                        Icons.star,
                        color: Colors.red,
                      ),
                      new Icon(
                        Icons.star,
                        color: Colors.red,
                      ),
                      new Icon(
                        Icons.star,
                        color: Colors.red,
                      ),
                    ],
                  ),
                ),
                new Container(
                  child: new Text(
                    '170 Revlews',
                    style: new TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ),
          new Container(
            padding: EdgeInsets.only(top: 30),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                _buildLeftItem(Icons.contacts, 'PREP:', '25 min'),
                _buildLeftItem(Icons.share, 'COOK:', '1 hr'),
                _buildLeftItem(Icons.phone, 'FEEDS:', '4-6'),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildLeftItem(IconData icon, String key, String value) {
    return new Container(
      child: new Column(
        children: <Widget>[
          new Container(
            child: new Icon(
              icon,
              color: Colors.green,
            ),
          ),
          new Container(
            child: new Text(
              key,
              style: new TextStyle(
                color: Colors.black,
                fontSize: 12,
              ),
            ),
          ),
          new Container(
            child: new Text(
              value,
              style: new TextStyle(
                color: Colors.black,
                fontSize: 12,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
