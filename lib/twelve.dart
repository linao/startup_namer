import 'package:flutter/material.dart';

class Twelve extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        body: new Container(
          padding: EdgeInsets.only(top: 80),
          child: new SizedBox(
            height: 310,
            child: new Card(
              elevation: 10,
              child: new Column(
                children: <Widget>[
                  //图片 + 标题
                  new Stack(
                    alignment: AlignmentDirectional.bottomStart,
                    children: <Widget>[
                      //图片
                      new Image.asset(
                        'images/green.jpg',
                        fit: BoxFit.contain,
                      ),
                      //标题
                      new Container(
                        padding: EdgeInsets.only(left: 10, bottom: 10),
                        child: new Text(
                          'Top 10 Australian beaches',
                          textAlign: TextAlign.end,
                          style: new TextStyle(
                            color: Color.fromARGB(255, 255, 255, 255),
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ],
                  ),
                  //分割线
                  new Divider(
                    height: 0.1,
                    color: Colors.black54,
                  ),
                  //内容
                  new Container(
                    padding: EdgeInsets.only(top: 10, left: 10),
                    alignment: Alignment.topLeft,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          'Number 10',
                          style: new TextStyle(
                            color: Color.fromARGB(255, 0, 0, 0),
                            fontSize: 15,
                          ),
                        ),
                        new Text(
                          'Whitehaven Beach',
                          style: new TextStyle(
                            color: Color.fromARGB(255, 0, 0, 0),
                            fontSize: 15,
                          ),
                        ),
                        new Text(
                          'Whitsunday Island,Whitsunday Islands',
                          style: new TextStyle(
                            color: Color.fromARGB(255, 0, 0, 0),
                            fontSize: 15,
                          ),
                        ),
                      ],
                    ),
                  ),
                  //蓝色按键
                  new Container(
                    margin: EdgeInsets.only(right: 190),
                    padding: EdgeInsets.only(
                      left: 10,
                      top: 15,
                    ),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text(
                          'SHARE',
                          style: new TextStyle(
                            color: Color.fromARGB(255, 0, 120, 255),
                            fontSize: 15,
                          ),
                        ),
                        new Text(
                          'EXPLORE',
                          style: new TextStyle(
                            color: Color.fromARGB(255, 0, 120, 255),
                            fontSize: 15,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
