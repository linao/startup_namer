import 'package:flutter/material.dart';

class Fourth extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
    decoration: new FlutterLogoDecoration(),
      child: new Center(
        child: new Text(
          'Hello World',
          textDirection: TextDirection.ltr,
          style: new TextStyle(
              fontSize: 40,
              color: Colors.white),
        ),
      ),
    );
  }
}
