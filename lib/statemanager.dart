import 'package:flutter/material.dart';

class StateManagerApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        body: ParentWidgetC(),
      ),
    );
  }
}

//****************自己管理状态
class _TapboxA extends StatefulWidget {
  @override
  _TapboxAState createState() => _TapboxAState();
}

class _TapboxAState extends State<_TapboxA> {
  bool _active = false;

  void _handleTap() {
    setState(() {
      _active = !_active;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      onTap: _handleTap,
      child: new Container(
        width: 200,
        height: 200,
        decoration: new BoxDecoration(
          color: _active ? Colors.lightGreen[700] : Colors.grey,
        ),
        child: new Center(
          child: new Text(
            _active ? 'Active' : "Inactive",
            style: new TextStyle(
              fontSize: 32,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}

//****************父管理状态
class ParentWidget extends StatefulWidget {
  @override
  _ParentWidgetState createState() => new _ParentWidgetState();
}

class _ParentWidgetState extends State<ParentWidget> {
  bool _active = false;

  void _handleTap(bool newValue) {
    print('TapboxB变了,接收： $newValue');
    setState(() {
      _active = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new _TapboxB(
        active: _active,
        onChanged: _handleTap,
      ),
    );
  }
}

class _TapboxB extends StatelessWidget {
  final bool active;
  final ValueChanged<bool> onChanged;

  _TapboxB({Key key, this.active: false, @required this.onChanged})
      : super(key: key);

  void _handleTap() {
    onChanged(!active);
    print('执行回调函数 onChanged，发出: ${!active}');
  }

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      onTap: _handleTap,
      child: new Container(
        decoration: new BoxDecoration(
          color: active ? Colors.lightGreen[700] : Colors.grey,
        ),
        width: 200,
        height: 200,
        child: new Center(
          child: new Text(
            active ? 'Active' : 'Inactive',
            style: new TextStyle(
              color: Colors.white,
              fontSize: 32,
            ),
          ),
        ),
      ),
    );
  }
}

//********************混合管理

class ParentWidgetC extends StatefulWidget {
  @override
  _ParentWidgetStateC createState() => new _ParentWidgetStateC();
}

class _ParentWidgetStateC extends State<ParentWidgetC> {

  bool _active = false;

  void _handleTapboxChanged(bool newValue){
    setState(() {
      _active = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new _TapboxC(
        active: _active,
        onChange: _handleTapboxChanged,
      ),
    );
  }
}

class _TapboxC extends StatefulWidget {
  final bool active;
  final ValueChanged<bool> onChange;

  _TapboxC({Key key, this.active: false, this.onChange});

  @override
  _TapboxCState createState() => new _TapboxCState();
}

class _TapboxCState extends State<_TapboxC> {
  bool _highlight = false;

  void _handleTapDown(TapDownDetails details) {
    setState(() {
      _highlight = true;
    });
  }

  void _handleTapUp(TapUpDetails details) {
    setState(() {
      _highlight = false;
    });
  }

  void _handleTapCancel() {
    setState(() {
      _highlight = false;
    });
  }

  void _handleTap() {
    widget.onChange(!widget.active);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new GestureDetector(
      onTapDown: _handleTapDown,
      onTapUp: _handleTapUp,
      onTapCancel: _handleTapCancel,
      onTap: _handleTap,
      child: new Container(
        width: 200,
        height: 200,
        decoration: new BoxDecoration(
          color: widget.active ? Colors.lightGreen[700] : Colors.grey,
          border: _highlight
              ? new Border.all(
                  color: Colors.teal[700],
                  width: 10,
                )
              : null,
        ),
      ),
    );
  }
}
