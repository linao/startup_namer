import 'package:flutter/material.dart';
import 'dart:developer';
import 'package:flutter/foundation.dart';

class StateApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new MaterialApp(
      home: new Scaffold(
        body: new Container(
          child: FavoriteWidget(),
        ),
      ),
    );
  }
}

class FavoriteWidget extends StatefulWidget {
  @override
  _FavoriteState createState() {
    // TODO: implement createState
    return new _FavoriteState();
  }
}

class _FavoriteState extends State<FavoriteWidget> {


  var _favorites = 41;
  bool _isFavorite = false;

  _onPress(){
    setState(() {
      if(_isFavorite){
        _isFavorite = false;
        _favorites -= 1;
      }else{
        _isFavorite = true;
        _favorites += 1;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: EdgeInsets.only(top: 30, left: 30),
      child: new Row(
        children: <Widget>[
          new Container(
            child: new Text('$_favorites'),
          ),
          new IconButton(
            icon: new Icon(
              _isFavorite ? Icons.star : Icons.star_border,
              color: Colors.red[500],
            ),
            onPressed: _onPress,
          ),
        ],
      ),
    );
  }

}

