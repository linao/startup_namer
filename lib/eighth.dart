import 'package:flutter/material.dart';

class Eighth extends StatelessWidget {
  String picUrl =
      'https://ss3.baidu.com/-fo3dSag_xI4khGko9WTAnF6hhy/image/h%3D300/sign=4cf62521bdde9c82b965ff8f5c8080d2/d1160924ab18972b0aa9c1d2e8cd7b899e510a13.jpg';

  @override
  Widget build(BuildContext context) {
    return _secondMethod();
  }

  //第一种方法，使用行 + 列实现表格，不能滚动
  MaterialApp _firstMethod() {
    return new MaterialApp(
      home: new Scaffold(
        body: new Container(
          decoration: new BoxDecoration(
            color: Colors.pinkAccent,
            border: new Border.all(
              color: Colors.black26,
            ),
          ),
          child: new Column(
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new Container(
                      decoration: new BoxDecoration(
                          border: new Border.all(
                            width: 10,
                            color: Colors.black54,
                          ),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(5),
                          )),
                      margin: const EdgeInsets.all(4),
                      child: new Image.network(picUrl),
                    ),
                  ),
                  new Expanded(
                    child: new Container(
                      decoration: new BoxDecoration(
                          border: new Border.all(
                            width: 10,
                            color: Colors.black54,
                          ),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(5),
                          )),
                      margin: const EdgeInsets.all(4),
                      child: new Image.network(picUrl),
                    ),
                  ),
                  new Expanded(
                    child: new Container(
                      decoration: new BoxDecoration(
                          border: new Border.all(
                            width: 10,
                            color: Colors.black54,
                          ),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(5),
                          )),
                      margin: const EdgeInsets.all(4),
                      child: new Image.network(picUrl),
                    ),
                  ),
                ],
              ),
              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new Container(
                      decoration: new BoxDecoration(
                          border: new Border.all(
                            width: 10,
                            color: Colors.black54,
                          ),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(5),
                          )),
                      margin: const EdgeInsets.all(4),
                      child: new Image.network(picUrl),
                    ),
                  ),
                  new Expanded(
                    child: new Container(
                      decoration: new BoxDecoration(
                          border: new Border.all(
                            width: 10,
                            color: Colors.black54,
                          ),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(5),
                          )),
                      margin: const EdgeInsets.all(4),
                      child: new Image.network(picUrl),
                    ),
                  ),
                  new Expanded(
                    child: new Container(
                      decoration: new BoxDecoration(
                          border: new Border.all(
                            width: 10,
                            color: Colors.black54,
                          ),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(5),
                          )),
                      margin: const EdgeInsets.all(4),
                      child: new Image.network(picUrl),
                    ),
                  ),
                ],
              ),
              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new Container(
                      decoration: new BoxDecoration(
                          border: new Border.all(
                            width: 10,
                            color: Colors.black54,
                          ),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(5),
                          )),
                      margin: const EdgeInsets.all(4),
                      child: new Image.network(picUrl),
                    ),
                  ),
                  new Expanded(
                    child: new Container(
                      decoration: new BoxDecoration(
                          border: new Border.all(
                            width: 10,
                            color: Colors.black54,
                          ),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(5),
                          )),
                      margin: const EdgeInsets.all(4),
                      child: new Image.network(picUrl),
                    ),
                  ),
                  new Expanded(
                    child: new Container(
                      decoration: new BoxDecoration(
                          border: new Border.all(
                            width: 10,
                            color: Colors.black54,
                          ),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(5),
                          )),
                      margin: const EdgeInsets.all(4),
                      child: new Image.network(picUrl),
                    ),
                  ),
                ],
              ),
              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new Container(
                      decoration: new BoxDecoration(
                          border: new Border.all(
                            width: 10,
                            color: Colors.black54,
                          ),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(5),
                          )),
                      margin: const EdgeInsets.all(4),
                      child: new Image.network(picUrl),
                    ),
                  ),
                  new Expanded(
                    child: new Container(
                      decoration: new BoxDecoration(
                          border: new Border.all(
                            width: 10,
                            color: Colors.black54,
                          ),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(5),
                          )),
                      margin: const EdgeInsets.all(4),
                      child: new Image.network(picUrl),
                    ),
                  ),
                  new Expanded(
                    child: new Container(
                      decoration: new BoxDecoration(
                          border: new Border.all(
                            width: 10,
                            color: Colors.black54,
                          ),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(5),
                          )),
                      margin: const EdgeInsets.all(4),
                      child: new Image.network(picUrl),
                    ),
                  ),
                ],
              ),
              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new Container(
                      decoration: new BoxDecoration(
                          border: new Border.all(
                            width: 10,
                            color: Colors.black54,
                          ),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(5),
                          )),
                      margin: const EdgeInsets.all(4),
                      child: new Image.network(picUrl),
                    ),
                  ),
                  new Expanded(
                    child: new Container(
                      decoration: new BoxDecoration(
                          border: new Border.all(
                            width: 10,
                            color: Colors.black54,
                          ),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(5),
                          )),
                      margin: const EdgeInsets.all(4),
                      child: new Image.network(picUrl),
                    ),
                  ),
                  new Expanded(
                    child: new Container(
                      decoration: new BoxDecoration(
                          border: new Border.all(
                            width: 10,
                            color: Colors.black54,
                          ),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(5),
                          )),
                      margin: const EdgeInsets.all(4),
                      child: new Image.network(picUrl),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  //第一种方法，使用 GridView 实现表格，能滚动
  MaterialApp _secondMethod() {
    return new MaterialApp(
      home: new Scaffold(
        body: _buildGrid(),
      ),
    );
  }

  List<Container> _buildFridTileList(int count) {
    return new List<Container>.generate(
        count,
            (int index) =>
        new Container(
          child: new Image.network(picUrl),
          color: Color.fromARGB(255, 255, 120, 120),
        ));
  }

  Widget _buildGrid() {
    return new GridView.extent(
      maxCrossAxisExtent: 150,
      padding: EdgeInsets.all(4),
      mainAxisSpacing: 4,
      crossAxisSpacing: 4,
      children: _buildFridTileList(100),
    );
  }
}
