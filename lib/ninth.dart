import 'package:flutter/material.dart';

class Ninth extends StatelessWidget {
  String picUrl =
      'https://ss1.baidu.com/-4o3dSag_xI4khGko9WTAnF6hhy/image/h%3D300/sign=7c2a0d1923f5e0fef1188f016c6034e5/d788d43f8794a4c2f12be52b00f41bd5ad6e39a1.jpg';

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        body: _buildGridView(),
      ),
    );
  }

  List<Container> _buildItems(int count, List items) {

    return new List<Container>.generate(
      count,
      (int index) => new Container(
            padding: EdgeInsets.all(4),
            child: new Stack(
              alignment: Alignment.bottomLeft,
              children: <Widget>[
                new Image.network(picUrl),
                new Container(
                  padding: EdgeInsets.only(left: 15, right: 15),
                  color: Color.fromARGB(127, 200, 200, 200),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          new Text(
                            items[index],
                            style: new TextStyle(
                              color: Colors.white,
                            ),
                            textAlign: TextAlign.start,
                          ),
                          new Text(
                            'alsdfas',
                            style: new TextStyle(
                              color: Colors.white,
                            ),
                            textAlign: TextAlign.start,
                          ),
                        ],
                      ),
                      new Container(
                        child: new Icon(
                          Icons.star,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
    );
  }

  GridView _buildGridView() {
    var items = List();
    items.add('asdfasf');
    items.add('sfdgsdfh');
    items.add('fhjghj');
    items.add('ery');
    items.add('fgjh');
    items.add('dfykjg');
    items.add('sgfh');
    items.add('sgj');
    items.add('sj');
    items.add('asdfasf');
    items.add('sfdgsdfh');
    items.add('fhjghj');
    items.add('ery');
    items.add('fgjh');
    items.add('dfykjg');
    items.add('sgfh');
    items.add('sgj');
    items.add('sj');
    items.add('asdfasf');
    items.add('sfdgsdfh');
    items.add('fhjghj');
    items.add('ery');
    items.add('fgjh');
    items.add('dfykjg');
    items.add('sgfh');
    items.add('sgj');
    items.add('sj');
    items.add('asdfasf');
    items.add('sfdgsdfh');
    items.add('fhjghj');
    items.add('ery');
    items.add('fgjh');
    items.add('dfykjg');
    items.add('sgfh');
    items.add('sgj');
    items.add('sj');
    return new GridView.count(
      crossAxisCount: 2,
      children: _buildItems(items.length, items),
    );
  }
}
