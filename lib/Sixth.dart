import 'package:flutter/material.dart';

class Sixth extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
          appBar: new AppBar(
            title: new Text('sixth'),
          ),
          body: new Container(
            child: new Row(
              children: <Widget>[
                new Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
                  new Icon(
                    Icons.star,
                    color: Colors.green,
                  ),
                  new Icon(
                    Icons.star,
                    color: Colors.green,
                  ),
                  new Icon(
                    Icons.star,
                    color: Colors.green,
                  ),
                  new Icon(
                    Icons.star,
                    color: Colors.green,
                  ),
                  new Icon(
                    Icons.star,
                    color: Colors.black,
                  ),
                ]),
                new Text(
                  '170 Reviews',
                  style: new TextStyle(
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
