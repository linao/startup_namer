import 'package:flutter/material.dart';

class Fifth extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'fifth',
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text('fifth'),
        ),
        body: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            new Expanded(
              child: new Image.asset('images/3.jpg'),
            ),
            new Expanded(
              child: new Image.asset('images/6.jpg'),
              flex: 2,
            ),
            new Expanded(
              child: new Image.asset('images/8.jpg'),
            ),
          ],
        ),
      ),
    );
  }
}
