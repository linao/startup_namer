import 'package:flutter/material.dart';

class Thrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget imageWidget = new Image.asset(
      'images/green.jpg',
      fit: BoxFit.cover,
    );

    Widget titleWidget = new Container(
      padding: const EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
      child: new Row(
        children: <Widget>[
          new Expanded(
              child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Text(
                'Oeschinen Lake Campground',
                style: new TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              new Text(
                'Kandersteg, Switzerland',
                style: new TextStyle(color: Colors.black38, fontSize: 15),
              ),
            ],
          )),
          new Icon(
            Icons.star,
            color: Colors.red,
            size: 18,
          ),
          new Text(
            '41',
            style: new TextStyle(
              color: Colors.black,
              fontSize: 15,
            ),
          ),
        ],
      ),
    );

    Widget buttonWidget = new Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          new Container(
            child: new Column(
              children: <Widget>[
                new Icon(
                  Icons.phone,
                  color: Colors.blue,
                ),
                new Container(
                  padding: EdgeInsets.only(top: 10),
                  child: new Text(
                    'CALL',
                    style: new TextStyle(color: Colors.blue, fontSize: 18),
                  ),
                )
              ],
            ),
          ),
          new Container(
            child: new Column(
              children: <Widget>[
                new Icon(
                  Icons.near_me,
                  color: Colors.blue,
                ),
                new Container(
                  padding: EdgeInsets.only(top: 10),
                  child: new Text(
                    'ROUTE',
                    style: new TextStyle(color: Colors.blue, fontSize: 18),
                  ),
                )
              ],
            ),
          ),
          new Container(
            child: new Column(
              children: <Widget>[
                new Icon(
                  Icons.share,
                  color: Colors.blue,
                ),
                new Container(
                  padding: EdgeInsets.only(top: 10),
                  child: new Text(
                    'SHARE',
                    style: new TextStyle(color: Colors.blue, fontSize: 18),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );

    Widget textWidget = new Container(
      padding: EdgeInsets.all(30),
      child: new Text('Lake Oeschinen lies at the foot of the '
          'Blüemlisalp in the Bernese Alps. '
          'Situated 1,578 meters above sea level,'
          ' it is one of the larger Alpine Lakes.'
          ' A gondola ride from Kandersteg, '
          'followed by a half-hour walk through '
          'pastures and pine forest, leads you to the lake'
          ', which warms to 20 degrees Celsius in the summer.'
          ' Activities enjoyed here include rowing, '
          'and riding the summer toboggan run.',

      ),
    );
    return new MaterialApp(
      title: 'three',
      home: new Scaffold(
        body: new ListView(
          children: <Widget>[
            imageWidget,
            titleWidget,
            buttonWidget,
            textWidget,
          ],
        ),
      ),
    );
  }
}
