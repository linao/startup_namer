import 'package:flutter/material.dart';

class Eleventh extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        body: new Container(
          child: new Stack(
            children: <Widget>[
              new Image.asset('images/green.jpg'),
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Container(
                    padding: EdgeInsets.only(left: 15, top: 40),
                    child: new Icon(
                      Icons.arrow_back_ios,
                      color: Color.fromARGB(255, 255, 255, 255),
                      size: 22,
                    ),
                  ),
                  new Container(
                    padding: EdgeInsets.only(right: 15),
                    child: new Row(
                      children: <Widget>[
                        new Container(
                          padding: EdgeInsets.only(left: 15, top: 40),
                          child: new Icon(
                            Icons.brush,
                            color: Color.fromARGB(255, 255, 255, 255),
                            size: 22,
                          ),
                        ),
                        new Container(
                          padding: EdgeInsets.only(left: 15, top: 40),
                          child: new Icon(
                            Icons.more_vert,
                            color: Color.fromARGB(255, 255, 255, 255),
                            size: 22,
                          ),
                        ),
                      ],
                    ),
                  ),

                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}