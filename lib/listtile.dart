import 'package:flutter/material.dart';

class Listtile extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        body: new Column(
          children: <Widget>[
            new ListTile(
              title: new Text('1625 Main Street'),
              subtitle: new Text('My City, CA 99987'),
              leading: new Icon(
                Icons.restaurant_menu,
                color: Colors.blue[400],
              ),
            ),
            new ListTile(
              title: new Text('1625 Main Street'),
              subtitle: new Text('My City, CA 99987'),
              leading: new Icon(
                Icons.restaurant_menu,
                color: Colors.blue[400],
              ),
            ),
            new ListTile(
              title: new Text('1625 Main Street'),
              subtitle: new Text('My City, CA 99987'),
              leading: new Icon(
                Icons.restaurant_menu,
                color: Colors.blue[400],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
